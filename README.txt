Read-only Gollum interface
==========================

This module provides a read-only interface to Github's Gollum Wiki system.

In brief the Gollum Wiki is a simple collection of Markdown (*.md) pages maintained
within a git repository.  

The full version of Gollum is written in Ruby and uses its own web server on a 
custom TCP port number.  That is not very good for integration with other systems,
so a small Drupal module was thought up to allow a completely seamless integration
with Drupal.

The module only allows the viewing of pages - editing must be either done through the
Gollum editor locally, or the files directly edited with a text editor.

Golum is available here: https://github.com/gollum/gollum

The module relies on Michelf's Markdown parsing system, available from here:
https://github.com/michelf/php-markdown

The Markdown package is linked from this repository as a sub-module for convenience.
It should be present as $PWD/php-markdown/Michelf/Markdown.inc.php and associated files.
